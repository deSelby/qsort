#include <stdio.h>
#include <stdlib.h>
#include "qsort_index.h"

static inline int sort_ascending( const unsigned int* a, const unsigned int* b, const double *data )
{
	const double firstDataValue = data[ *a ];
	const double secondDataValue = data[ *b ];

	if( firstDataValue < secondDataValue ) return 1;
	else return 0;
}

int main()
{
	srand( 1 );

	const int arraySize = 100;
	double data[ arraySize ];
	int index[ arraySize ];

	for( int i = 0; i < arraySize; i++ ) {
		//Fill index array with consecutive numbers
		index[ i ] = i;
		//Fill data array with random data
		data[ i ] = rand() / 1000.f;
	}

	//Sort indexes based on lowest data
	QSORT_INDEX( double, data, index, arraySize, sort_ascending );

	//Display ten lowest numbers
	for( int i = 0; i < 10; i++ ) {
		printf("Index position: %d, data: %f\n", index[ i ], data[ index[ i ] ] );
	}
}
