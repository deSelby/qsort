QSORT\_INDEX
=========
A version of qsort that sorts an array of indices based on the values from an additional data array; for use with Data Oriented Programming.

Usage
---------
You feed the function two arrays; one containing data that needs to be sorted and another one containing a list of unsigned integers representing index values in the first array.   
Instead of sorting the data array it sorts the indices based on some criteria.

Say you have these two arrays:

| Data | Index |
|:---:|:---:|
| 4.82 | 0 |
| 2.6 | 1 |
| 9.12 | 2 |

You want to be able to go through the data array in order of largest numbers but you don't want to change the order of those numbers. After using QSORT\_INDEX only the index array will be reordered to look like this:  
2  
0  
1

Indicating that the the index point 2 in the data array, holding value 9.12, is the largest number.

Description of how the function works:

QSORT\_INDEX( type, data\_array, index\_array, array\_size, comparison\_function )

type: The type of a single element in the data array. So for example could be int, double, char\* e.t.c.  
data\_array: The name of the array which you want to sort by. In the above example this would be the *a* array.  
index\_array: An unsigned int array containing numbers 0 through N - 1 where N is the number of elements in the data array.  
array\_size: The number of elements in both data and index arrays.  
comparison\_function: The name of a function doing the comparison that the arrays will be fed through. Needs to be defines as "static inline int function\_name( const unsigned int\*, const unsigned int\*, type\* )" where type is the same as above. Can only return 0 or 1.

An example of a comparison function that sorts by largest value:

	int compare( const unsigned int *a, const unsigned int *b, const double *data )
	{
		double firstValue = data[ *a ];
		double secondValue = data[ *b ];

		if( firstValue > secondValue ) return 1;
		else return 0;
	}

"data" is a pointer to the first element of the data array. "a" and "b" are the two integers from the index array. Values are pulled from the data array at indicies "a" and "b" and compared, "a" and "b" are then sorted based on which values in the data array was the largest. The function can only return 1 or 0.

Raison d'etre
---------
When sorting in C you of course got qsort(), but qsort is really built around a more traditional object oriented approach where you're sorting an array of struct pointers; which becomes a problem when doing DOP. When the data is split into multiple arrays you can't just pick one array and sort it as that would leave the other arrays unsorted and the order of the elements would no longer correspond to one another.

Let's say you want to sort objects based on one of their properties and you have an object defined by a struct like so:

	struct object {
		double a;
		bool b;
		char c;
	};

Then you have an array of pointers to a bunch of these object structs and you might want to find the *c* and *b* values of the object with the lowest *a* value.  
Normally this is easy, you just call qsort() and tell it to sort your array of struct pointers based on the *a* member variable of those structs but what if instead of a struct containing a mix of a, b and c values you're using DOP and have three separate arrays containing groups of those values:

	double *a;
	bool *b;
	char *c;

The first object would consist of the first value from each of the three arrays, the fifth object would be a combination of the fifth value from the three arrays e.t.c.    
You can't just feed qsort the array containing *a* values since it will just rearrange that array and the first object in it will no longer relate to the first object in the *b* and *c* arrays. So you won't know which *b* and *c* values belongs to the object with the lowest *a* value.    

What to do is to take an auxiliary int array of of numbers representing indices - say 0 to 99 if you had 100 objects - and then sort them based on some other array. You could then take your rearranged array of indices and use that to step through all the other arrays.    
Before the sort the index array would contain integers in numerical order so 0, 1, 2 ,3, e.t.c. representing the order of the objects in all three arrays *a*, *b* and *c*. After the sort the order of the indices in the array will reflect the sorting criterion. For example if you sorted based on lowest values in the *a* array and then after the sort have number 42 as the first number in the index array then that means that the object on the 42nd index position in arrays *a*, *b* and *c* is the one with the lowest *a* value.
